package canvas

import "bitbucket.org/clayts/gla"

var aabProgram = gla.NewProgram(`
#version 330

in vec2 source1;
in vec2 destination1;
in vec2 source2;
in vec2 destination2;
in float depth;

out vec2 vSource1;
out vec2 vSource2;
//implicit: out vec4 gl_Position
out vec2 vDestination1;
out vec2 vDestination2;
out float vDepth;


void main() {
vSource1 = source1/2+0.5;
vSource2 = source2/2+0.5;
vDestination1 = destination1;
vDestination2 = destination2;
vDepth = depth;
}
`, `
#version 330


layout(points) in;
layout(triangle_strip,max_vertices = 4) out;

in vec2 vSource1[];
in vec2 vSource2[];
in vec2 vDestination1[];
in vec2 vDestination2[];
in float vDepth[];

out vec2 gSource;

uniform vec2 xbasis;
uniform vec2 ybasis;
uniform vec2 translation;


//(t.x.X()*v.X())+(t.y.X()*v.Y())+t.t.X(),
//(t.x.Y()*v.X())+(t.y.Y()*v.Y())+t.t.Y(),
//(mat2(xbasis,ybasis)*(            ))+translation

void main()
{

gl_Position = vec4((mat2(xbasis,ybasis)*vDestination1[0])+translation, vDepth[0], 1);
gSource = vSource1[0];
EmitVertex();

gl_Position = vec4((mat2(xbasis,ybasis)*vec2(vDestination1[0].x,vDestination2[0].y))+translation, vDepth[0], 1);
gSource = vec2(vSource1[0].x,vSource2[0].y);
EmitVertex();

gl_Position = vec4((mat2(xbasis,ybasis)*vec2(vDestination2[0].x,vDestination1[0].y))+translation, vDepth[0], 1);
gSource = vec2(vSource2[0].x,vSource1[0].y);
EmitVertex();

gl_Position = vec4((mat2(xbasis,ybasis)*vDestination2[0])+translation, vDepth[0], 1);
gSource = vSource2[0];
EmitVertex();

EndPrimitive();
}

`, `
#version 330

uniform sampler2D tex;
uniform float discardAlpha;

in vec2 gSource;


out vec4 output;

void main() {
vec4 texel = texture(tex, vec2(gSource.x, 1-gSource.y));
if (discardAlpha == 1.0) {
  if(texel.a < 1.0) {
  	discard;
  }
} else {
  // texel.a *= 0.9;
	texel.a = gl_FragCoord.z;
}
output = texel;
}
`)

type aabSubCanvas struct {
	lineSubCanvas
}

func newAABSubCanvas() *aabSubCanvas {
	t := &aabSubCanvas{}
	t.data = make(subCanvasData)
	t.cSize = 9
	t.program = aabProgram
	return t
}
