package canvas

import (
	"bitbucket.org/clayts/geometry"
	"bitbucket.org/clayts/gla"
)

type multiCanvas struct {
	triangles *triangleSubCanvas
	lines     *lineSubCanvas
	circles   *circleSubCanvas
	points    *pointSubCanvas
	aabs      *aabSubCanvas
	alphDscrd float32
}

func newMultiCanvas() *multiCanvas {
	mc := &multiCanvas{}
	mc.triangles = newTriangleSubCanvas()
	mc.lines = newLineSubCanvas()
	mc.circles = newCircleSubCanvas()
	mc.points = newPointSubCanvas()
	mc.aabs = newAABSubCanvas()
	return mc
}

func (mc *multiCanvas) Render(src geometry.Transform) {
	render(mc.triangles, src, mc.alphDscrd)
	render(mc.lines, src, mc.alphDscrd)
	render(mc.circles, src, mc.alphDscrd)
	render(mc.points, src, mc.alphDscrd)
	render(mc.aabs, src, mc.alphDscrd)
}

func (mc *multiCanvas) clear() {
	mc.triangles.clear()
	mc.lines.clear()
	mc.circles.clear()
	mc.points.clear()
	mc.aabs.clear()
}

func (mc *multiCanvas) paintConvex(tex *gla.Texture, src, dst geometry.Transform, p geometry.Shape, depth float32) {
	fvi := p.FirstVertexIndex()
	fv := p.Vertex(fvi)
	ai := p.NextVertexIndexClockwise(fvi)
	if ai == fvi {
		sv, dv := src.Transformed(fv), dst.Transformed(fv)
		mc.points.add(tex, sv.X(), sv.Y(), dv.X(), dv.Y(), depth)
		return
	}

	a := p.Vertex(ai)
	bi := p.NextVertexIndexClockwise(ai)
	b := p.Vertex(bi)
	svf, dvf := src.Transformed(fv), dst.Transformed(fv)
	for {
		sva, dva := src.Transformed(a), dst.Transformed(a)
		svb, dvb := src.Transformed(b), dst.Transformed(b)
		mc.triangles.add(tex, svf.X(), svf.Y(), dvf.X(), dvf.Y(),
			sva.X(), sva.Y(), dva.X(), dva.Y(),
			svb.X(), svb.Y(), dvb.X(), dvb.Y(), depth)
		ai, a = bi, b
		bi = p.NextVertexIndexClockwise(ai)
		if bi == fvi {
			break
		}
		b = p.Vertex(bi)
	}
}

func (mc *multiCanvas) paint(tex *gla.Texture, src, dst geometry.Transform, sh geometry.Shape, depth float32) {
	srcS := geometry.NewTransformShape(sh, src)
	dstS := geometry.NewTransformShape(sh, dst)
	switch dstS.ShapeType() {
	case geometry.AABType:
		lb, rt := dstS.Vertex(int(geometry.LB)), dstS.Vertex(int(geometry.RT))
		slb, srt := srcS.Vertex(int(geometry.LB)), srcS.Vertex(int(geometry.RT))
		mc.aabs.add(tex, slb.X(), slb.Y(), lb.X(), lb.Y(), srt.X(), srt.Y(), rt.X(), rt.Y(), depth)
	case geometry.CircleType:
		srcBox := srcS.BoundingBox()
		dstBox := dstS.BoundingBox()
		a := -geometry.FirstVertexAngle(srcS) + geometry.FirstVertexAngle(dstS)
		mc.circles.add(tex, srcBox.Centre().X(), srcBox.Centre().Y(), dstBox.Centre().X(), dstBox.Centre().Y(), srcBox.RadiusX(), dstBox.RadiusX(), a, depth)
	case geometry.ConcaveType:
		cci := dstS.FirstConvexComponentIndex()
		ccistart := cci
		for {
			mc.paint(tex, src, dst, dstS.ConvexComponent(cci), depth)
			cci = dstS.NextConvexComponentIndex(cci)
			if cci == ccistart {
				break
			}
		}
	case geometry.LineType:
		ai := dstS.FirstVertexIndex()
		a := dstS.Vertex(ai)
		bi := dstS.NextVertexIndexClockwise(ai)
		b := dstS.Vertex(bi)
		at := srcS.Vertex(ai)
		bt := srcS.Vertex(bi)
		mc.lines.add(tex, at.X(), at.Y(), a.X(), a.Y(), bt.X(), bt.Y(), b.X(), b.Y(), depth)
	default:
		mc.paintConvex(tex, src, dst, sh, depth)
	}
}
