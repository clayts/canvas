package canvas

import (
	"bitbucket.org/clayts/geometry"
	"bitbucket.org/clayts/gla"
	"github.com/go-gl/gl/all-core/gl"
)

type Canvas struct {
	text              *textCanvas
	base              *multiCanvas
	lights            *multiCanvas
	glows             *multiCanvas
	fullscreenCanvas  *aabSubCanvas
	fullscreenTexture *gla.Texture
	lightsActive      bool
}

func (cvs *Canvas) LightAll() {
	cvs.lightsActive = false
}

//NewCanvas returns a pointer to a new Canvas
func NewCanvas() *Canvas {
	cvs := &Canvas{}
	cvs.text = newTextCanvas()
	cvs.glows = newMultiCanvas()
	cvs.glows.alphDscrd = 0
	cvs.base = newMultiCanvas()
	cvs.base.alphDscrd = 1
	cvs.lights = newMultiCanvas()
	cvs.lights.alphDscrd = 0
	cvs.lightsActive = true
	cvs.fullscreenCanvas = newAABSubCanvas()
	cvs.fullscreenTexture = gla.NewBlankTexture(geometry.NewVector(1, 1), true)
	// cvs.lightCanvas.add(texs, sx1, sy1, dx1, dy1, sx2, sy2, dx2, dy2, depth)
	cvs.fullscreenCanvas.add(cvs.fullscreenTexture, -1, -1, -1, -1, 1, 1, 1, 1, 1)
	// prevF := gla.OnResize
	// gla.OnResize = func(size geometry.Vector) {
	// 	cvs.fullscreenTexture.Replace(gla.NewBlankTexture(size, true))
	// 	// cvs.lightCanvas.add(cvs.lightTex, -1, -1, -1, -1, 1, 1, 1, 1, 1)
	// 	if prevF != nil {
	// 		prevF(size)
	// 	}
	// }
	return cvs
}

func (cvs *Canvas) Write(t Chars, location geometry.Vector, width float32, height float32, depth float32, alignment geometry.Corner, justification float32) {
	// t, area, depth, alignment, justification := wrt.Text(), wrt.Area(), wrt.Depth(), wrt.Alignment(), wrt.Justification()
	cvs.text.data = append(cvs.text.data, txtData{location, width, height, alignment, justification, t, depth})
}

//Render renders what has been painted on the Canvas
func (cvs *Canvas) Render(cameraTransform geometry.Transform, windowRegion geometry.AAB, win *gla.Window) {
	if cvs.fullscreenTexture.BoundingBox().Size() != win.Size() {
		cvs.fullscreenTexture.Replace(gla.NewBlankTexture(win.Size(), true))
	}
	win.Capture(windowRegion, func() {
		gl.ClearColor(0, 0, 0, 0)
		gla.ClearColourAndDepth()

		//BASE
		gl.Disable(gl.BLEND)
		gl.Enable(gl.DEPTH_TEST)
		gl.DepthFunc(gl.LESS)
		cvs.base.Render(cameraTransform)

		//LIGHTS
		if cvs.lightsActive {
			cvs.fullscreenTexture.Capture(func() {
				gla.ClearColourAndDepth()
				gl.Enable(gl.BLEND)
				gl.BlendFunc(gl.ONE, gl.ONE)
				gl.Disable(gl.DEPTH_TEST)
				cvs.lights.Render(cameraTransform)
			})
			gl.BlendFunc(gl.ZERO, gl.SRC_COLOR)
			render(cvs.fullscreenCanvas, geometry.NewScaleTransform(geometry.Vector{1, -1}), 0)
		}
		//GLOWS
		cvs.fullscreenTexture.Capture(func() {
			gla.ClearColourAndDepth()
			gl.Enable(gl.BLEND)
			gl.BlendFunc(gl.SRC_ALPHA, gl.ONE)
			gl.Disable(gl.DEPTH_TEST)
			cvs.glows.Render(cameraTransform)
		})
		gl.BlendFunc(gl.ONE, gl.ONE_MINUS_SRC_COLOR)
		render(cvs.fullscreenCanvas, geometry.NewScaleTransform(geometry.Vector{1, -1}), 0)

		//TEXT
		cvs.text.sub.clear()
		cvs.text.render(windowRegion.Radius(), cameraTransform, win)
		gl.BlendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA)
		render(cvs.text.sub, geometry.IdentityTransform, 0)
	})
}

// type Applicable interface {
// 	Apply(*Canvas)
// }
//
// type Writing struct {
// 	Text          Chars
// 	Location      geometry.Vector
// 	Width, Height float32
// 	Depth         float32
// 	Alignment     geometry.Corner
// 	Justification float32
// }
//
// func NewWriting(text Chars, location geometry.Vector, width, height, depth float32, alignment geometry.Corner, justification float32) Writing {
// 	return Writing{text, location, width, height, depth, alignment, justification}
// }
//
// func (wrt Writing) Apply(cvs *Canvas) {
// 	cvs.Write(wrt.Text, wrt.Location, wrt.Width, wrt.Height, wrt.Depth, wrt.Alignment, wrt.Justification)
// }

//
// func (wrt Writing) Text() Chars                { return wrt.text }
// func (wrt Writing) Depth() float32             { return wrt.depth }
// func (wrt Writing) Alignment() geometry.Corner { return wrt.alignment }
// func (wrt Writing) Justification() float32     { return wrt.justification }

// type Animation struct {
// 	Frames []struct {
// 		File string
// 		Src  geometry.Transform
// 	}
// 	Smooth bool
// 	Wrap   gla.TextureOverflowOperation
// 	Dst    geometry.Transform
// 	Sh     geometry.Shape
// 	Depth  float32
// }
//
// func NewAnimation(smooth bool, wrap gla.TextureOverflowOperation, dst geometry.Transform, sh geometry.Shape, depth float32) Animation {
// 	return Animation{nil, smooth, wrap, dst, sh, depth}
// }
//
// func (anim Animation) Painting(i int) Painting {
// 	return NewPainting(anim.Frames[i].File, anim.Smooth, anim.Wrap, anim.Frames[i].Src, anim.Dst, anim.Sh, anim.Depth)
// }
//
// type Painting struct {
// 	File     string
// 	Smooth   bool
// 	Wrap     gla.TextureOverflowOperation
// 	Src, Dst geometry.Transform
// 	Sh       geometry.Shape
// 	Depth    float32
// }
//
// func NewPainting(file string, smooth bool, wrap gla.TextureOverflowOperation, src, dst geometry.Transform, p geometry.Shape, depth float32) Painting {
// 	return Painting{file, smooth, wrap, src, dst, p, depth}
// }
//
// func (pnt Painting) Apply(cvs *Canvas) {
// 	cvs.Paint(pnt.File, pnt.Smooth, pnt.Wrap, pnt.Src, pnt.Dst, pnt.Sh, pnt.Depth)
// }

// func (pnt Painting) Source() TextureSource       { return pnt.src }
// func (pnt Painting) Destination() geometry.Shape { return pnt.dst }
// func (pnt Painting) Depth() float32              { return pnt.dpth }

func (cvs *Canvas) Paint(tex *gla.Texture, src, dst geometry.Transform, sh geometry.Shape, depth float32) {
	cvs.base.paint(tex, src, dst, sh, depth)
}

func (cvs *Canvas) Light(tex *gla.Texture, src, dst geometry.Transform, sh geometry.Shape) {
	cvs.lights.paint(tex, src, dst, sh, 0)
}

func (cvs *Canvas) Glow(tex *gla.Texture, src, dst geometry.Transform, sh geometry.Shape, opacity float32) {
	cvs.glows.paint(tex, src, dst, sh, opacity)
}

//Clear clears the Canvas
func (cvs *Canvas) Clear() {
	cvs.lightsActive = true
	cvs.base.clear()
	cvs.lights.clear()
	cvs.glows.clear()
	cvs.text.Clear()
}
