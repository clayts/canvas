package canvas

import (
	"fmt"

	"bitbucket.org/clayts/geometry"
	"bitbucket.org/clayts/gla"
)

var ScreenAAB = geometry.AABRadiusOne

func NewOrthographicTransform(camera geometry.AAB) geometry.Transform {
	return geometry.NewTransposedTransform(
		1.0/(camera.RadiusX()),
		0.0,
		-(camera.R()+camera.L())/(camera.Width()),
		0.0,
		1.0/(camera.RadiusY()),
		-(camera.T()+camera.B())/(camera.Height()),
	)
	// )
}

func Outline(sh geometry.Shape, thickness float32) []geometry.Shape {
	var answer []geometry.Shape
	geometry.EachEdge(sh, func(l geometry.Line) bool {
		answer = append(answer,
			geometry.NewPolygonFromLineThickness(l, thickness),
			geometry.NewCircleFromCentreRadiusAngle(l.End(), thickness/2, 0))
		return true
	})
	return answer
}

func printChars(css ...Chars) {
	for _, cs := range css {
		fmt.Print("'")
		for _, c := range cs {
			fmt.Print(string(c.r))
		}
		fmt.Println("'")
	}
}

func GetImagePixelTransform(file string) geometry.Transform {
	return geometry.NewAABToAABTransform(gla.GetImageBoundingBox(file), ScreenAAB)
}

func nextPowerOf2(n int) int {
	i := 1
	for i <= n {
		i *= 2
	}
	return i
}
