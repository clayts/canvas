package canvas

import (
	"image"
	"image/color"
	"io/ioutil"

	"bitbucket.org/clayts/geometry"
	"bitbucket.org/clayts/gla"
	"github.com/go-gl/mathgl/mgl32"
	"github.com/golang/freetype/truetype"
	"golang.org/x/image/font"
	"golang.org/x/image/math/fixed"
)

//Font contains data necessary to render fonts and layout text in OpenGL
type Font struct {
	face        font.Face
	rgba        *image.RGBA
	texAreas    map[rune]geometry.AAB
	tex         *gla.Texture
	colour      mgl32.Vec4
	size        float32
	hyphenWidth fixed.Int26_6
	descent     fixed.Int26_6
}

//NewFont returns a new pointer to a Font
func NewFont(file string, size float32, r, g, b, a float32) *Font {
	f := &Font{}
	f.colour = mgl32.Vec4{r, g, b, a}
	f.size = size
	f.texAreas = make(map[rune]geometry.AAB)
	fileBytes, err := ioutil.ReadFile(file)
	if err != nil {
		panic(err)
	}
	tt, err := truetype.Parse(fileBytes)
	if err != nil {
		panic(err)
	}
	renderFace := truetype.NewFace(tt, &truetype.Options{
		Size:    float64(f.size),
		DPI:     72,
		Hinting: font.HintingFull,
	})

	texSize := nextPowerOf2(int(f.size * 32))

	fg := &image.Uniform{color.RGBA{uint8(f.colour[0] * 255), uint8(f.colour[1] * 255), uint8(f.colour[2] * 255), uint8(f.colour[3] * 255)}}
	f.rgba = image.NewRGBA(image.Rect(0, 0, texSize, texSize))

	d := &font.Drawer{
		Dst:  f.rgba,
		Src:  fg,
		Face: renderFace,
	}

	startX, startY := f.size, f.size
	x, y := startX, startY

	var col int
	for i := 32; i < 128; i++ {
		char := rune(i)
		d.Dot = fixed.P(int(x), int(y))
		d.DrawString(string(char))

		l := ((x) - f.size) / float32(texSize)
		b := 1 - ((y)+f.size)/float32(texSize)
		r := ((x) + f.size) / float32(texSize)
		t := 1 - ((y)-f.size)/float32(texSize)

		f.texAreas[char] = geometry.NewAABFromLBRT((l*2)-1, (b*2)-1, (r*2)-1, (t*2)-1)
		col++
		if col == 16 {
			col = 0
			x = startX
			y += f.size * 2
		} else {
			x += f.size * 2
		}
	}
	// myfile, _ := os.Create("test.png")
	// png.Encode(myfile, f.rgba)
	// os.Exit(0)
	f.size = size
	f.face = truetype.NewFace(tt, &truetype.Options{
		Size:    float64(f.size),
		DPI:     72,
		Hinting: font.HintingFull,
	})
	f.hyphenWidth = font.MeasureString(f.face, "-")
	f.descent = f.face.Metrics().Descent
	f.tex = gla.NewTextureFromRGBA(f.rgba, false)
	return f
}

//Sprite returns the sprite of the character
func (fnt *Font) sprite(char rune) (*gla.Texture, geometry.AAB) {
	if a, ok := fnt.texAreas[char]; ok {
		return fnt.tex, a
	}
	panic("no such glyph " + string(char))
}

func reversed(cs Chars) Chars {
	l := len(cs)
	answer := make(Chars, l)
	for i, c := range cs {
		answer[l-1-i] = c
	}
	return answer
}
