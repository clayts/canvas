package canvas

import (
	"fmt"

	"bitbucket.org/clayts/geometry"
	"bitbucket.org/clayts/gla"
)

func main() {
	fnt := NewFont("font.ttf", 26, 0, 0, 1, 1)
	txt := fnt.NewChars("hello world")
	cvs := NewCanvas()
	// pS := NewSprite(geometry.NewVector(0.1, 0.5), "texture.png", false, gla.Discard)
	// crcS := NewSprite(geometry.NewCircle(0.5, 0.5, 0.5, 0), "texture.png", false, gla.Discard)
	// s := NewSprite(geometry.NewPolygon(
	// 	0, 0,
	// 	0, .5,
	// 	0.5, 1,
	// 	0.6, 0.9,
	// 	0.55, 1,
	// 	1, 0.5,
	// 	1, 0), "texture.png", false, gla.Discard)
	tex := gla.NewTextureFromFile("texture.png", false)
	tex1 := gla.NewTextureFromFile("white.png", false)
	tex2 := gla.NewTextureFromFile("firepalette.png", false)
	tex4 := gla.NewTextureFromColour(1, 0, 1, 1, false)
	fmt.Println(geometry.NewPolygonFromVectors(geometry.NewVector(0, 0), geometry.NewVector(0, 10), geometry.NewVector(3, 3), geometry.NewVector(10, 0)))
	cvs.Paint(tex, geometry.IdentityTransform, geometry.IdentityTransform, geometry.NewPolygon(
		0, 0,
		0, .5,
		0.5, 1,
		0.6, 0.9,
		0.55, 1,
		1, 0.5,
		1, 0), 0)
	cvs.Paint(tex, geometry.IdentityTransform, geometry.IdentityTransform, geometry.NewPolygonFromVectors(
		geometry.NewVector(0.05, 0.8),
		geometry.NewVector(0.09, 0.7),
	), 0)
	// spew.Dump(cvs)
	cvs.Paint(tex4, geometry.IdentityTransform, geometry.IdentityTransform, geometry.NewCircle(0.2, 0.9, 0.1, 0), 0)
	cvs.Paint(tex, geometry.IdentityTransform, geometry.IdentityTransform, geometry.NewVector(0.05, 0.9), 0)

	cvs.Write(txt, geometry.NewVector(0.2, 0.9), 0.8, 0.1, -0.5, geometry.LB, 0)

	cvs.Paint(tex4, geometry.IdentityTransform, geometry.IdentityTransform, geometry.NewAABFromLBRT(0.9, 0.9, 1, 1), 0)

	cvs.Light(tex1, geometry.IdentityTransform, geometry.IdentityTransform, geometry.NewAABFromLBRT(0, 0, 0.3, 1))
	cvs.Glow(tex2, geometry.IdentityTransform, geometry.IdentityTransform, geometry.NewAABFromLBRT(0, 0, 1, 0.3), 0.3)
	// cvs.SetLighting(false)
	cam := NewOrthographicTransform(geometry.NewAABFromLBRT(-0.1, -0.1, 1.1, 1.1))
	gla.CreateWindow("Canvas Test", geometry.Vector{800, 800}, func(win *gla.Window) bool {
		cam = cam.FollowedBy(geometry.NewRotationTransform(0.01))
		cvs.Render(cam, ScreenAAB, win)
		return true
	})
}
