package canvas

import (
	"fmt"

	"golang.org/x/image/math/fixed"
)

//NewChars creates character strings which can be displayed
func (fnt *Font) NewChars(is ...interface{}) Chars {
	var answer Chars

	for _, iface := range is {
		if cs, ok := iface.(Chars); ok {
			answer = append(answer, cs...)
		} else if c, ok := iface.(Char); ok {
			answer = append(answer, c)
		} else {
			str := fmt.Sprint(iface)
			for i, r := range str {
				c := fnt.newChar(r)
				if i < len(str)-1 {
					c.width += c.Kern(rune(str[i+1]))
				}
				answer = append(answer, c)
			}
		}
	}

	return answer
}

//Descent returns the maximum descent of the string of characters
func (cs Chars) Descent() float32 {
	var answer fixed.Int26_6
	for _, c := range cs {
		if c.fnt.descent > answer {
			answer = c.fnt.descent
		}
	}
	return float32(answer.Round())
}

//
// type fontSprite struct {
// 	tex  string
// 	area geometry.AAB
// }

//
// func (fs fontSprite) TextureRegion() geometry.Shape         { return fs.area }
// func (fs fontSprite) Texture() string                       { return fs.tex }
// func (fs fontSprite) Smoothed() bool                        { return true }
// func (fs fontSprite) Wrapped() gla.TextureOverflowOperation { return gla.Discard }
//Chars contain a string of characters
type Chars []Char

//Width returns the width of a string of characters as a float32
func (cs Chars) Width() float32 {
	return float32(cs.WidthFixed().Round())
}

//WidthFixed returns the width of a string of characters as a fixed.Int26_6
func (cs Chars) WidthFixed() fixed.Int26_6 {
	var answer fixed.Int26_6
	for _, c := range cs {
		answer += c.WidthFixed()
	}
	return answer
}

//Height returns the height of a string of characters
func (cs Chars) Height() float32 {
	var answer float32
	for _, c := range cs {
		if c.Height() > answer {
			answer = c.Height()
		}
	}
	return answer
}

//Count returns the number of characters matching the given letter
func (cs Chars) Count(r rune) int {
	var answer int
	for _, c := range cs {
		if c.r == r {
			answer++
		}
	}
	return answer
}

func (cs Chars) String() string {
	var answer string
	for _, c := range cs {
		answer += string(c.r)
	}
	return answer
}

//CharAt returns the character overlapping, or closest to, the given distance from the start of the string of characters
func (cs Chars) CharAt(position fixed.Int26_6) int {
	var width fixed.Int26_6
	for i, c := range cs {
		width += c.WidthFixed()
		if width > position {
			return i
		}
	}
	return len(cs) - 1
}

func (cs Chars) next(i int, r rune) int {
	for ; i < len(cs); i++ {
		if cs[i].r == r {
			return i
		}
	}
	return -1
}

func (cs Chars) prev(i int, r rune) int {
	for ; i >= 0; i-- {
		if cs[i].r == r {
			return i
		}
	}
	return -1
}

//Descent returns the character's font's descent as a float32
func (c Char) Descent() float32 {
	return float32(c.fnt.descent.Round())
}

const minHypenationWidthFactor = float32(0) //TODO Remove???!?

func truncate(cs Chars, pos int, cutoffPx float32, maxWidth fixed.Int26_6) Chars {
	if cs[pos].WidthFixed() < maxWidth {
		if cs[pos].Height() < cutoffPx {
			return cs[:pos]
		}
	}

	end := cs[pos-1]
	dot := end.fnt.newChar('.')
	elWidth := dot.WidthFixed() + dot.WidthFixed() + dot.WidthFixed()
	if maxWidth < dot.WidthFixed() {
		return nil
	}
	if maxWidth < dot.WidthFixed()+dot.WidthFixed() {
		return Chars{dot}
	}
	if maxWidth < elWidth {
		return Chars{dot, dot}
	}
	newEnd := cs[:pos].CharAt(maxWidth - elWidth)
	if cs[newEnd].r == '\n' {
		return end.fnt.NewChars(cs[:newEnd], dot, dot, dot, "\n")
	}
	return end.fnt.NewChars(cs[:newEnd], dot, dot, dot)
}

//if given zero space, returns whole line
func forEachWrapped(cs Chars, maxWidth fixed.Int26_6, cutoffPx float32, f func(cs Chars)) {
	if maxWidth == 0 && cutoffPx == 0 {
		f(cs)
		return
	}
	var width fixed.Int26_6
	if len(cs) > 0 {
		if cs[0].r == ' ' {
			cs = cs[1:]
		}
	}
	for i, c := range cs {
		width += c.WidthFixed()
		if cutoffPx-c.Height() < 0 {
			return
		}
		if c.r == '\n' {
			h := cutoffPx - cs[:i+1].Height()
			f(truncate(cs, i+1, h, maxWidth))
			forEachWrapped(cs[i+1:], maxWidth, h, f)
			return
		}
		if width > maxWidth && c.r != ' ' {
			if i == 0 {
				return
			}
			lastSpace := cs.prev(i, ' ')
			if lastSpace > 0 && cs[:lastSpace].Width()/float32(maxWidth.Round()) > 0.7 && cs[:lastSpace].Count(' ') > 0 { //ratio of text : linewidth > x && at least 2 words on line
				h := cutoffPx - cs[:lastSpace].Height()
				f(truncate(cs, lastSpace, h, maxWidth))
				forEachWrapped(cs[lastSpace+1:], maxWidth, h, f)
				return
			}
			//no suitable break point
			if cs[:i].Count(' ') > 0 {
				nextSpace := cs.next(i, ' ')
				if nextSpace != -1 {
					wordLen := nextSpace - lastSpace
					wordCent := (lastSpace + wordLen/2) + 1
					if wordCent < i {
						hPoint := cs[:wordCent].CharAt(maxWidth - c.fnt.hyphenWidth)
						if hPoint > 0 && float32(maxWidth.Round()) > c.fnt.size*minHypenationWidthFactor {
							line := c.fnt.NewChars(cs[:hPoint], "-", cs[hPoint])
							h := cutoffPx - line.Height()
							f(truncate(line, hPoint+1, h, maxWidth))
							forEachWrapped(cs[hPoint:], maxWidth, h, f)
							return
						}
					}
				}
			}

			hPoint := cs[:i].CharAt(maxWidth - c.fnt.hyphenWidth)
			if hPoint > 0 && cs[hPoint-1].r != ' ' && cs[hPoint].r != ' ' && float32(maxWidth.Round()) > c.fnt.size*minHypenationWidthFactor {
				line := c.fnt.NewChars(cs[:hPoint], "-", cs[hPoint])
				h := cutoffPx - line.Height()
				f(truncate(line, hPoint+1, h, maxWidth))
				forEachWrapped(cs[hPoint:], maxWidth, h, f)
				return
			}
			if cs[i-1].r == ' ' {
				h := cutoffPx - cs[:i-1].Height()
				f(truncate(cs, i-1, h, maxWidth))
				forEachWrapped(cs[i:], maxWidth, h, f)
				return
			}
			if lastSpace > 0 {
				h := cutoffPx - cs[:lastSpace].Height()
				f(truncate(cs, lastSpace, h, maxWidth))
				forEachWrapped(cs[lastSpace+1:], maxWidth, h, f)
				return
			}
			h := cutoffPx - cs[:i].Height()
			f(truncate(cs, i, h, maxWidth))
			forEachWrapped(cs[i:], maxWidth, h, f)
			return
		}
	}
	f(cs)
	return
}

//Equals returns true if the two strings of characters have the same contents
func (cs Chars) Equals(cs2 Chars) bool {
	if len(cs) != len(cs2) {
		return false
	}
	for i, c := range cs {
		if c != cs2[i] {
			return false
		}
	}
	return true
}
