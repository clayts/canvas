package canvas

import (
	"bitbucket.org/clayts/gla"
	"github.com/go-gl/gl/all-core/gl"
)

var pointProgram = gla.NewProgram(`
#version 330


in vec2 source1;
in vec2 destination1;
in float depth;

out vec2 vSource1;
//implicit: out vec4 gl_Position
out vec2 vDestination1;
out float vDepth;

void main() {

vSource1 = source1/2+0.5;
vDestination1 = destination1;
vDepth = depth;
}
`, `
#version 330


uniform vec2 xbasis;
uniform vec2 ybasis;
uniform vec2 translation;

layout(points) in;
layout(points, max_vertices = 1) out;

in vec2 vSource1[];
in vec2 vDestination1[];
in float vDepth[];
//(mat2(xbasis,ybasis)*(            ))+translation

out vec2 gSource;
void main()
{
gl_Position=vec4((mat2(xbasis,ybasis)*vDestination1[0])+translation, vDepth[0], 1);
gSource = vSource1[0];
EmitVertex();

EndPrimitive();
}

`, `
#version 330

uniform sampler2D tex;
uniform float discardAlpha;
in vec2 gSource;

out vec4 output;

void main() {
vec4 texel = texture(tex, vec2(gSource.x, 1-gSource.y));
if (discardAlpha == 1.0) {
  if(texel.a < 1.0) {
  	discard;
  }
} else {
  // texel.a *= 0.9;
	texel.a = gl_FragCoord.z;
}
output = texel;
}
`)

type pointSubCanvas struct {
	triangleSubCanvas
}

func (t *pointSubCanvas) textureData(texs *gla.Texture) renderData {
	var data renderData
	if data = t.data[texs]; !data.setup {

		data.setup = true
		//Points
		gl.GenVertexArrays(1, &data.vao)
		gl.BindVertexArray(data.vao)

		gl.GenBuffers(1, &data.vbo)
		gl.BindBuffer(gl.ARRAY_BUFFER, data.vbo)

		//sx1, sy1, dx1, dy1, depth
		gl.EnableVertexAttribArray(t.program.Attribute("source1"))
		gl.VertexAttribPointer(t.program.Attribute("source1"), 2, gl.FLOAT, false, t.cSize*gla.FloatSize, gl.PtrOffset(0*gla.FloatSize))

		gl.EnableVertexAttribArray(t.program.Attribute("destination1"))
		gl.VertexAttribPointer(t.program.Attribute("destination1"), 2, gl.FLOAT, false, t.cSize*gla.FloatSize, gl.PtrOffset(2*gla.FloatSize))

		gl.EnableVertexAttribArray(t.program.Attribute("depth"))
		gl.VertexAttribPointer(t.program.Attribute("depth"), 1, gl.FLOAT, false, t.cSize*gla.FloatSize, gl.PtrOffset(4*gla.FloatSize))

		t.data[texs] = data
	}
	return data
}

func (t *pointSubCanvas) add(texs *gla.Texture, sx1, sy1, dx1, dy1, depth float32) {
	data := t.data[texs]
	data.fs = append(data.fs, sx1, sy1, dx1, dy1, depth)
	t.data[texs] = data
}

func newPointSubCanvas() *pointSubCanvas {
	t := &pointSubCanvas{}
	t.data = make(subCanvasData)
	t.cSize = 5
	t.program = pointProgram

	return t
}
