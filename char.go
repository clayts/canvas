package canvas

import (
	"bitbucket.org/clayts/geometry"
	"bitbucket.org/clayts/gla"
	"golang.org/x/image/math/fixed"
)

//Char represents a character
type Char struct {
	fnt   *Font
	r     rune
	width fixed.Int26_6
}

//Height returns the character's height
func (c Char) Height() float32 { return c.fnt.size }

func (fnt *Font) newChar(r rune) Char {
	c := Char{}
	c.r = r
	c.fnt = fnt
	if c.r == '-' {
		c.width = fnt.hyphenWidth
	} else if c.r == '\n' {
		c.width = 0
	} else {
		var ok bool
		c.width, ok = c.fnt.face.GlyphAdvance(c.r)
		if !ok {
			panic("no such glyph " + string(c.r))
		}
	}
	return c
}

//Kern returns the kerning value for a given character, given the following character
func (c Char) Kern(r rune) fixed.Int26_6 {
	return c.fnt.face.Kern(c.r, r)
}

//Sprite returns the character's sprite
func (c Char) sprite() (*gla.Texture, geometry.AAB) {
	return c.fnt.sprite(c.r)
}

//Width returns the character's width as a float32
func (c Char) Width() float32 {
	return float32(c.WidthFixed().Round())
}

//WidthFixed returns the character's width as a fixed.Int26_6
func (c Char) WidthFixed() fixed.Int26_6 {
	return c.width
}
