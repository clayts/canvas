package canvas

import (
	"bitbucket.org/clayts/geometry"
	"bitbucket.org/clayts/gla"
	"github.com/go-gl/mathgl/mgl32"
	"golang.org/x/image/math/fixed"
)

type textCanvas struct {
	data []txtData
	sub  *textSubCanvas
}

func newTextCanvas() *textCanvas {
	t := &textCanvas{}
	t.sub = newTextSubCanvas()
	return t
}

type txtData struct {
	location      geometry.Vector
	width, height float32
	alignment     geometry.Corner
	justification float32
	text          Chars
	depth         float32
}

func (tc *textCanvas) render(dstSize geometry.Vector, tfm geometry.Transform, win *gla.Window) {
	winSize := win.Size()

	resSize := mgl32.Vec2{dstSize.X() * winSize.X(), dstSize.Y() * winSize.Y()}
	for _, d := range tc.data {
		if len(d.text) == 0 {
			continue
		}
		if d.width == 0 {
			d.justification = 0
		}
		// area = geometry.NewAABFromCentreRadius(area.Centre().Minus(sCent), area.Radius())
		scale := tfm.Scale()
		area := geometry.NewAABFromCornerWidthHeight(tfm.Transformed(d.location).TimesX(resSize.X()/2).TimesY(resSize.Y()/2), d.alignment, d.width*scale.X()*resSize.X(), d.height*scale.Y()*resSize.Y())
		// l := area.L() / sSize.X()
		// b := area.B() / sSize.Y()
		// r := area.R() / sSize.X()
		// t := area.T() / sSize.Y()
		// if !geometry.Intersects(geometry.NewAABFromLBRT(l, b, r, t), geometry.AABUnitAtOrigin) {
		// 	continue
		// }
		// l *= resSize.X()
		// b *= resSize.Y()
		// r *= resSize.X()
		// t *= resSize.Y()
		// area = geometry.NewAABFromLBRT(l, b, r, t)

		maxWidth := fixed.I(int(area.Width()))
		// maxWidth := fixed.I(int(round(area.Width())))
		startCorner := area.Corner(d.alignment)
		if d.alignment == geometry.LT {
			cursor := startCorner
			cutoffPx := area.Height()
			firstLine := true
			forEachWrapped(d.text, maxWidth, cutoffPx, func(l Chars) {
				if firstLine {
					cursor = cursor.PlusY(l.Descent())
					firstLine = false
				}
				lHeight := l.Height()
				cursor = geometry.Vector{startCorner.X(), cursor.Y()}
				cursor = cursor.MinusY(lHeight)
				pad := d.justification
				if pad != 0 {
					pad *= (area.Width() - l.Width()) / float32(l.Count(' '))
				}
				if len(l) > 0 {
					if l[len(l)-1].r == '\n' {
						pad = 0
					}
				}
				for _, c := range l {
					if c.r == '\n' {
					} else if c.r == ' ' {
						cursor = cursor.PlusX(c.Width() + pad)
					} else {
						tc.renderChar(c, cursor, d.depth, resSize)
						cursor = cursor.PlusX(c.Width())
					}
				}
			})
		} else if d.alignment == geometry.LB {
			text := reversed(d.text)
			cursor := startCorner
			cutoffPx := area.Height()
			firstLine := true
			forEachWrapped(text, maxWidth, cutoffPx, func(l Chars) {
				if firstLine {
					cursor = cursor.PlusY(l.Descent())
					firstLine = false
				}
				lHeight := l.Height()
				cursor = geometry.Vector{startCorner.X(), cursor.Y()}
				pad := d.justification
				if pad != 0 {
					pad *= (area.Width() - l.Width()) / float32(l.Count(' '))
				}
				if len(l) > 0 {
					if l[len(l)-1].r == '\n' {
						pad = 0
					}
				}
				for i := len(l) - 1; i >= 0; i-- {
					c := l[i]
					if c.r == '\n' {
					} else if c.r == ' ' {
						cursor = cursor.PlusX(c.Width() + pad)
					} else {
						tc.renderChar(c, cursor, d.depth, resSize)
						cursor = cursor.PlusX(c.Width())
					}
				}
				cursor = cursor.PlusY(lHeight)

			})
		} else if d.alignment == geometry.RT {
			startCorner = area.Corner(geometry.LT)
			cursor := startCorner
			cutoffPx := area.Height()
			firstLine := true
			forEachWrapped(d.text, maxWidth, cutoffPx, func(l Chars) {
				if firstLine {
					cursor = cursor.PlusY(l.Descent())
					firstLine = false
				}
				lHeight := l.Height()
				cursor = geometry.Vector{startCorner.X(), cursor.Y()}
				cursor = cursor.MinusY(lHeight)
				lWidth := l.Width()
				totalPadding := d.justification
				pad := totalPadding
				if totalPadding != 0 {
					totalPadding *= (area.Width() - lWidth)
					if len(l) > 0 {
						if l[len(l)-1].r == '\n' {
							totalPadding = 0
						}
					}
					pad = totalPadding / float32(l.Count(' '))
				}
				alignPad := area.Width() - totalPadding - lWidth
				cursor = cursor.PlusX(alignPad)
				for _, c := range l {
					if c.r == '\n' {
					} else if c.r == ' ' {
						cursor = cursor.PlusX(c.Width() + pad)
					} else {
						tc.renderChar(c, cursor, d.depth, resSize)
						cursor = cursor.PlusX(c.Width())
					}
				}
			})
		} else if d.alignment == geometry.RB {
			startCorner = area.Corner(geometry.LB)
			text := reversed(d.text)
			cursor := startCorner
			cutoffPx := area.Height()
			firstLine := true
			forEachWrapped(text, maxWidth, cutoffPx, func(l Chars) {
				if firstLine {
					cursor = cursor.PlusY(l.Descent())
					firstLine = false
				}
				lHeight := l.Height()
				cursor = geometry.Vector{startCorner.X(), cursor.Y()}
				lWidth := l.Width()
				totalPadding := d.justification
				pad := totalPadding
				if totalPadding != 0 {
					totalPadding *= (area.Width() - lWidth)
					if len(l) > 0 {
						if l[len(l)-1].r == '\n' {
							totalPadding = 0
						}
					}
					pad = totalPadding / float32(l.Count(' '))
				}
				alignPad := area.Width() - totalPadding - lWidth
				cursor = cursor.PlusX(alignPad)
				if len(l) > 0 {
					if l[len(l)-1].r == '\n' {
						pad = 0
					}
				}
				for i := len(l) - 1; i >= 0; i-- {
					c := l[i]
					if c.r == '\n' {
					} else if c.r == ' ' {
						cursor = cursor.PlusX(c.Width() + pad)
					} else {
						tc.renderChar(c, cursor, d.depth, resSize)
						cursor = cursor.PlusX(c.Width())
					}
				}
				cursor = cursor.PlusY(lHeight)
			})
		} else {
			//TODO centre
			panic("no such alignment yet implemented")
		}
	}
}

func (tc *textCanvas) renderChar(c Char, positionPx geometry.Vector, depth float32, resSize mgl32.Vec2) {
	rh := c.Height()
	l := positionPx.X() - rh
	b := positionPx.Y() - rh
	r := l + rh*2
	t := b + rh*2

	lb, rt := geometry.NewVector(l/resSize.X(), b/resSize.Y()), geometry.NewVector(r/resSize.X(), t/resSize.Y())
	file, area := c.sprite()
	slb, srt := area.Vertex(int(geometry.LB)), area.Vertex(int(geometry.RT))
	tc.sub.add(file, slb.X(), slb.Y(), lb.X(), lb.Y(), srt.X(), srt.Y(), rt.X(), rt.Y(), depth)
}

func (tc *textCanvas) Clear() {
	tc.data = tc.data[:0]
}
