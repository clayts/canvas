package canvas

import (
	"bitbucket.org/clayts/gla"
	"github.com/go-gl/gl/all-core/gl"
)

type triangleSubCanvas struct {
	data    subCanvasData
	program *gla.Program
	cSize   int32
}

var triProgram = gla.NewProgram(`
#version 330


in vec2 source1;
in vec2 destination1;
in vec2 source2;
in vec2 destination2;
in vec2 source3;
in vec2 destination3;
in float depth;

out vec2 vSource1;
out vec2 vSource2;
out vec2 vSource3;
//implicit: out vec4 gl_Position
out vec2 vDestination1;
out vec2 vDestination2;
out vec2 vDestination3;
out float vDepth;

void main() {

vSource1 = source1/2+0.5;
vSource2 = source2/2+0.5;
vSource3 = source3/2+0.5;
vDestination1 = destination1;
vDestination2 = destination2;
vDestination3 = destination3;
vDepth = depth;
}
`, `
#version 330


uniform vec2 xbasis;
uniform vec2 ybasis;
uniform vec2 translation;

layout(points) in;
layout(triangle_strip,max_vertices = 3) out;

in vec2 vSource1[];
in vec2 vSource2[];
in vec2 vSource3[];
in vec2 vDestination1[];
in vec2 vDestination2[];
in vec2 vDestination3[];
in float vDepth[];

out vec2 gSource;

//(mat2(xbasis,ybasis)*(            ))+translation


void main()
{

gl_Position = vec4((mat2(xbasis,ybasis)*(vDestination1[0]))+translation, vDepth[0], 1);
gSource = vSource1[0];
EmitVertex();

gl_Position = vec4((mat2(xbasis,ybasis)*(vDestination2[0]))+translation, vDepth[0], 1);
gSource = vSource2[0];
EmitVertex();


gl_Position = vec4((mat2(xbasis,ybasis)*(vDestination3[0]))+translation, vDepth[0], 1);
gSource = vSource3[0];
EmitVertex();

EndPrimitive();
}

`, `
#version 330

uniform sampler2D tex;
uniform float discardAlpha;

in vec2 gSource;

out vec4 output;

void main() {
vec4 texel = texture(tex, vec2(gSource.x, 1-gSource.y));
if (discardAlpha == 1.0) {
  if(texel.a < 1.0) {
  	discard;
  }
} else {
  // texel.a *= 0.9;
	texel.a = gl_FragCoord.z;
}

output = texel;
}
`)

func newTriangleSubCanvas() *triangleSubCanvas {
	t := &triangleSubCanvas{}
	t.data = make(subCanvasData)
	t.cSize = 13
	t.program = triProgram
	return t
}

func (t *triangleSubCanvas) chunkSize() int32 {
	return t.cSize
}

func (t *triangleSubCanvas) renderProgram() *gla.Program {
	return t.program
}

func (t *triangleSubCanvas) renderData() subCanvasData {
	return t.data
}

func (t *triangleSubCanvas) clear() {
	for tex, data := range t.data {
		if len(data.fs) == 0 {
			delete(t.data, tex)
		} else {
			data.fs = data.fs[:0]
			t.data[tex] = data
		}
	}
}

func (t *triangleSubCanvas) textureData(texs *gla.Texture) renderData {
	var data renderData
	if data = t.data[texs]; !data.setup {
		data.setup = true
		//Triangles
		gl.GenVertexArrays(1, &data.vao)
		gl.BindVertexArray(data.vao)

		gl.GenBuffers(1, &data.vbo)
		gl.BindBuffer(gl.ARRAY_BUFFER, data.vbo)

		//sx1, sy1, dx1, dy1, sx2, sy2, dx2, dy2, sx3, sy3, dx3, dy3, depth
		gl.EnableVertexAttribArray(t.program.Attribute("source1"))
		gl.VertexAttribPointer(t.program.Attribute("source1"), 2, gl.FLOAT, false, t.cSize*gla.FloatSize, gl.PtrOffset(0*gla.FloatSize))

		gl.EnableVertexAttribArray(t.program.Attribute("destination1"))
		gl.VertexAttribPointer(t.program.Attribute("destination1"), 2, gl.FLOAT, false, t.cSize*gla.FloatSize, gl.PtrOffset(2*gla.FloatSize))

		gl.EnableVertexAttribArray(t.program.Attribute("source2"))
		gl.VertexAttribPointer(t.program.Attribute("source2"), 2, gl.FLOAT, false, t.cSize*gla.FloatSize, gl.PtrOffset(4*gla.FloatSize))

		gl.EnableVertexAttribArray(t.program.Attribute("destination2"))
		gl.VertexAttribPointer(t.program.Attribute("destination2"), 2, gl.FLOAT, false, t.cSize*gla.FloatSize, gl.PtrOffset(6*gla.FloatSize))

		gl.EnableVertexAttribArray(t.program.Attribute("source3"))
		gl.VertexAttribPointer(t.program.Attribute("source3"), 2, gl.FLOAT, false, t.cSize*gla.FloatSize, gl.PtrOffset(8*gla.FloatSize))

		gl.EnableVertexAttribArray(t.program.Attribute("destination3"))
		gl.VertexAttribPointer(t.program.Attribute("destination3"), 2, gl.FLOAT, false, t.cSize*gla.FloatSize, gl.PtrOffset(10*gla.FloatSize))

		gl.EnableVertexAttribArray(t.program.Attribute("depth"))
		gl.VertexAttribPointer(t.program.Attribute("depth"), 1, gl.FLOAT, false, t.cSize*gla.FloatSize, gl.PtrOffset(12*gla.FloatSize))

		t.data[texs] = data
	}
	return data
}

func (t *triangleSubCanvas) add(texs *gla.Texture, sx1, sy1, dx1, dy1, sx2, sy2, dx2, dy2, sx3, sy3, dx3, dy3, depth float32) {
	data := t.data[texs]
	data.fs = append(data.fs, sx1, sy1, dx1, dy1, sx2, sy2, dx2, dy2, sx3, sy3, dx3, dy3, depth)
	t.data[texs] = data
}
