package canvas

import (
	"bitbucket.org/clayts/geometry"
	"bitbucket.org/clayts/gla"
	"github.com/go-gl/gl/all-core/gl"

	_ "image/png"
)

type sortFS struct {
	chunkSize int
	depthPos  int
	fs        []float32
}

func (sfs sortFS) Len() int {
	return len(sfs.fs) / sfs.chunkSize
}

func (sfs sortFS) Less(i, j int) bool {
	return sfs.fs[(i*sfs.chunkSize)+sfs.depthPos] < sfs.fs[(j*sfs.chunkSize)+sfs.depthPos]
}

func (sfs sortFS) Swap(i, j int) {
	for k := 0; k < sfs.chunkSize; k++ {
		ip, jp := (i*sfs.chunkSize)+k, (j*sfs.chunkSize)+k
		sfs.fs[ip], sfs.fs[jp] = sfs.fs[jp], sfs.fs[ip]
	}
}

type subCanvas interface {
	renderData() subCanvasData
	renderProgram() *gla.Program
	chunkSize() int32
	textureData(*gla.Texture) renderData
}

func render(t subCanvas, tfm geometry.Transform, alphaDiscard float32) {
	var progSetup bool
	for texStr := range t.renderData() {
		data := t.textureData(texStr)
		tex := texStr.GL()
		if len(data.fs) > 0 {
			// fmt.Println("rendering", texStr)
			sfs := sortFS{int(t.chunkSize()), int(t.chunkSize() - 1), data.fs}
			// sort.Sort(sfs)
			if !progSetup {
				progSetup = true
				gl.UseProgram(t.renderProgram().Program())

				gl.Uniform2f(t.renderProgram().Uniform("xbasis"), tfm.XBasisX(), tfm.XBasisY())
				gl.Uniform2f(t.renderProgram().Uniform("ybasis"), tfm.YBasisX(), tfm.YBasisY())
				gl.Uniform2f(t.renderProgram().Uniform("translation"), tfm.TranslationX(), tfm.TranslationY())

				gl.Uniform1i(t.renderProgram().Uniform("tex"), 0)

				gl.Uniform1f(t.renderProgram().Uniform("alphaDiscard"), alphaDiscard)
			}
			gl.ActiveTexture(gl.TEXTURE0)
			gl.BindTexture(gl.TEXTURE_2D, tex)
			gl.BindBuffer(gl.ARRAY_BUFFER, data.vbo)
			if data.vboSize < len(sfs.fs) {
				gl.BufferData(gl.ARRAY_BUFFER, len(sfs.fs)*gla.FloatSize, gl.Ptr(t.renderData()[texStr].fs), gl.STATIC_DRAW)
				data.vboSize = len(sfs.fs)
				t.renderData()[texStr] = data
			} else {
				gl.BufferSubData(gl.ARRAY_BUFFER, 0, len(sfs.fs)*gla.FloatSize, gl.Ptr(t.renderData()[texStr].fs))
			}
			gl.BindVertexArray(data.vao)
			gl.DrawArrays(gl.POINTS, 0, int32(len(sfs.fs))/t.chunkSize())
		}
	}
}

//
// func (t *triangleSubCanvas) render(source geometry.AAB) {
// 	var progSetup bool
// 	for texStr, data := range t.data {
// 		tex := GetTexture(texStr)
// 		if len(data.fs) > 0 {
// 			if !progSetup {
// 				progSetup = true
// 				gl.UseProgram(t.program.Program())
// 				if source != geometry.AABZero {
// 					gl.Uniform4f(t.program.Uniform("camera"), source.CentreX(), source.CentreY(), source.RadiusX(), source.RadiusY())
// 				}
// 				gl.Uniform1i(t.program.Uniform("tex"), 0)
// 			}
// 			gl.ActiveTexture(gl.TEXTURE0)
// 			gl.BindTexture(gl.TEXTURE_2D, tex)
// 			gl.BindBuffer(gl.ARRAY_BUFFER, data.vbo)
// 			if data.vboSize < len(data.fs) {
// 				gl.BufferData(gl.ARRAY_BUFFER, len(data.fs)*gla.FloatSize, gl.Ptr(t.data[texStr].fs), gl.STATIC_DRAW)
// 				data.vboSize = len(data.fs)
// 				t.data[texStr] = data
// 			} else {
// 				gl.BufferSubData(gl.ARRAY_BUFFER, 0, len(data.fs)*gla.FloatSize, gl.Ptr(t.data[texStr].fs))
// 			}
// 			gl.BindVertexArray(data.vao)
// 			gl.DrawArrays(gl.POINTS, 0, int32(len(data.fs))/t.cSize)
// 		}
// 	}
// }

type renderData struct {
	fs       []float32
	vao, vbo uint32
	vboSize  int
	setup    bool
}

type subCanvasData map[*gla.Texture]renderData

//
// type TextureSource interface {
// 	TextureRegion() geometry.Shape
// 	Texture() string
// 	Smoothed() bool
// 	Wrapped() gla.TextureOverflowOperation
// }

//
// type Sprite struct {
// 	src    geometry.Shape
// 	tex    string
// 	smooth bool
// 	wrap   gla.TextureOverflowOperation
// }
//
// func (s Sprite) TextureRegion() geometry.Shape         { return s.src }
// func (s Sprite) Texture() string                       { return s.tex }
// func (s Sprite) Smoothed() bool                        { return s.smooth }
// func (s Sprite) Wrapped() gla.TextureOverflowOperation { return s.wrap }
// func NewSprite(src geometry.Shape, tex string, smooth bool, wrap gla.TextureOverflowOperation) Sprite {
// 	s := Sprite{src, tex, smooth, wrap}
// 	return s
// }
// func NewSpriteFromRegion(src geometry.Shape, box geometry.AAB, tex string, smooth bool, wrap gla.TextureOverflowOperation) Sprite {
// 	s := Sprite{geometry.NewTransformShape(src, geometry.NewAABToAABTransform(src.BoundingBox(), box)), tex, smooth, wrap}
// 	return s
// }
// func NewSpriteFromEntireImage(src geometry.Shape, tex string, smooth bool, wrap gla.TextureOverflowOperation) Sprite {
// 	return NewSpriteFromRegion(src, geometry.AABRadiusOne, tex, smooth, wrap)
// }

// func setupForRendering(destination geometry.AAB, dstSize, winSize geometry.Vector) {
//
// }
