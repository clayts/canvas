package canvas

import (
	"bitbucket.org/clayts/gla"
	"github.com/go-gl/gl/all-core/gl"
)

type circleSubCanvas struct {
	triangleSubCanvas
}

var circleProgram = gla.NewProgram(`
#version 330


in vec2 source1;
in vec2 destination1;
in float sourceRad;
in float destinationRad;
in float rotation;
in float depth;

out vec2 vSource1;
out float vSourceRad;
//implicit: out vec4 gl_Position
out vec2 vDestination1;
out float vDestinationRad;
out float vRotation;
out float vDepth;

void main() {

vSource1 = (source1/2)+0.5;
vSourceRad = sourceRad/2;
vDestination1 = destination1;
vDestinationRad = destinationRad;
vRotation = rotation;
vDepth = depth;
}
`, `
#version 330


uniform vec2 xbasis;
uniform vec2 ybasis;
uniform vec2 translation;

layout(points) in;
layout(triangle_strip,max_vertices = 4) out;

in vec2 vSource1[];
in float vSourceRad[];
in vec2 vDestination1[];
in float vDestinationRad[];
in float vDepth[];
in float vRotation[];

out vec2 gSource;
out vec2 gRelPos;
out vec2 cnt;
out float fRotation;

//(mat2(xbasis,ybasis)*(            ))+translation


void main()
{

fRotation = vRotation[0];
cnt = vSource1[0];
gRelPos = vec2(-1, -1);
gl_Position=vec4((mat2(xbasis,ybasis)*(vDestination1[0] + gRelPos*vDestinationRad[0]))+translation, vDepth[0], 1);
gSource = vSource1[0]+(gRelPos*vSourceRad[0]);
EmitVertex();

gRelPos = vec2(-1, +1);
gl_Position=vec4((mat2(xbasis,ybasis)*(vDestination1[0] + gRelPos*vDestinationRad[0]))+translation, vDepth[0], 1);
gSource = vSource1[0]+(gRelPos*vSourceRad[0]);
EmitVertex();

gRelPos = vec2(+1, -1);
gl_Position=vec4((mat2(xbasis,ybasis)*(vDestination1[0] + gRelPos*vDestinationRad[0]))+translation, vDepth[0], 1);
gSource = vSource1[0]+(gRelPos*vSourceRad[0]);
EmitVertex();

gRelPos = vec2(+1, +1);
gl_Position=vec4((mat2(xbasis,ybasis)*(vDestination1[0] + gRelPos*vDestinationRad[0]))+translation, vDepth[0], 1);
gSource = vSource1[0]+(gRelPos*vSourceRad[0]);
EmitVertex();



EndPrimitive();
}

`, `
#version 330

uniform sampler2D tex;

in vec2 gSource;
in vec2 gRelPos;
in vec2 cnt;
in float fRotation;
uniform float discardAlpha;

out vec4 output;

void main() {

if(pow(gRelPos.x,2)+pow(gRelPos.y,2) > 1)
discard;

float s = sin(-fRotation);
float c = cos(-fRotation);

vec4 texel = texture(tex, (vec2(0,1)-(vec2(((gSource.x-cnt.x)*c)-((gSource.y-cnt.y)*s), ((gSource.x-cnt.x)*s)+((gSource.y-cnt.y)*c))+cnt))*vec2(-1,1));
if (discardAlpha == 1.0) {
  if(texel.a < 1.0) {
  	discard;
  }
} else {
  // texel.a *= 0.9;
	texel.a = gl_FragCoord.z;
}
output = texel;
}
`)

func (t *circleSubCanvas) textureData(texs *gla.Texture) renderData {
	var data renderData
	if data = t.data[texs]; !data.setup {
		data.setup = true
		//Circles
		gl.GenVertexArrays(1, &data.vao)
		gl.BindVertexArray(data.vao)

		gl.GenBuffers(1, &data.vbo)
		gl.BindBuffer(gl.ARRAY_BUFFER, data.vbo)

		//sx1, sy1, dx1, dy1, sRad, dRad, rot, depth
		gl.EnableVertexAttribArray(t.program.Attribute("source1"))
		gl.VertexAttribPointer(t.program.Attribute("source1"), 2, gl.FLOAT, false, t.cSize*gla.FloatSize, gl.PtrOffset(0*gla.FloatSize))

		gl.EnableVertexAttribArray(t.program.Attribute("destination1"))
		gl.VertexAttribPointer(t.program.Attribute("destination1"), 2, gl.FLOAT, false, t.cSize*gla.FloatSize, gl.PtrOffset(2*gla.FloatSize))

		gl.EnableVertexAttribArray(t.program.Attribute("sourceRad"))
		gl.VertexAttribPointer(t.program.Attribute("sourceRad"), 1, gl.FLOAT, false, t.cSize*gla.FloatSize, gl.PtrOffset(4*gla.FloatSize))

		gl.EnableVertexAttribArray(t.program.Attribute("destinationRad"))
		gl.VertexAttribPointer(t.program.Attribute("destinationRad"), 1, gl.FLOAT, false, t.cSize*gla.FloatSize, gl.PtrOffset(5*gla.FloatSize))
		//
		gl.EnableVertexAttribArray(t.program.Attribute("rotation"))
		gl.VertexAttribPointer(t.program.Attribute("rotation"), 1, gl.FLOAT, false, t.cSize*gla.FloatSize, gl.PtrOffset(6*gla.FloatSize))

		gl.EnableVertexAttribArray(t.program.Attribute("depth"))
		gl.VertexAttribPointer(t.program.Attribute("depth"), 1, gl.FLOAT, false, t.cSize*gla.FloatSize, gl.PtrOffset(7*gla.FloatSize))

		t.data[texs] = data

	}
	return data
}

func (t *circleSubCanvas) add(texs *gla.Texture, sx1, sy1, dx1, dy1, sRad, dRad, rot, depth float32) {
	data := t.data[texs]
	data.fs = append(data.fs, sx1, sy1, dx1, dy1, sRad, dRad, rot, depth)
	t.data[texs] = data
}

func newCircleSubCanvas() *circleSubCanvas {
	t := &circleSubCanvas{}
	t.data = make(subCanvasData)
	t.cSize = 8
	t.program = circleProgram
	return t
}
