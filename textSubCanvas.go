package canvas

import "bitbucket.org/clayts/gla"

var textProgram = gla.NewProgram(`
#version 330

in vec2 source1;
in vec2 destination1;
in vec2 source2;
in vec2 destination2;
in float depth;

out vec2 vSource1;
out vec2 vSource2;
//implicit: out vec4 gl_Position
out vec2 vDestination1;
out vec2 vDestination2;
out float vDepth;

void main() {

vSource1 = source1/2+0.5;
vSource2 = source2/2+0.5;
vDestination1 = destination1;
vDestination2 = destination2;
vDepth = depth;
}
`, `
#version 330

layout(points) in;
layout(triangle_strip,max_vertices = 4) out;

in vec2 vSource1[];
in vec2 vSource2[];
in vec2 vDestination1[];
in vec2 vDestination2[];
in float vDepth[];

out vec2 gSource;

void main()
{
gl_Position = vec4(vDestination1[0]*2, vDepth[0], 1);
gSource = vSource1[0];
EmitVertex();

gl_Position = vec4(vec2(vDestination1[0].x,vDestination2[0].y)*2, vDepth[0], 1);
gSource = vec2(vSource1[0].x,vSource2[0].y);
EmitVertex();

gl_Position = vec4(vec2(vDestination2[0].x,vDestination1[0].y)*2, vDepth[0], 1);
gSource = vec2(vSource2[0].x,vSource1[0].y);
EmitVertex();

gl_Position = vec4(vDestination2[0]*2, vDepth[0], 1);
gSource = vSource2[0];
EmitVertex();

EndPrimitive();
}

`, `
#version 330

uniform sampler2D tex;


in vec2 gSource;

out vec4 output;

void main() {
vec4 texel = texture(tex, vec2(gSource.x, 1-gSource.y));
if(texel.a == 0)
discard;
output = texel;
}
`)

type textSubCanvas struct {
	aabSubCanvas
}

func newTextSubCanvas() *textSubCanvas {
	t := &textSubCanvas{}
	t.cSize = 9
	t.data = make(subCanvasData)
	t.program = textProgram

	return t
}
